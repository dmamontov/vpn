```
port 13555
proto udp
dev tap

ca /etc/openvpn/ca.crt
cert /etc/openvpn/server.crt
key /etc/openvpn/server.key
dh /etc/openvpn/dh.pem

server-bridge 192.168.50.10 255.255.255.0 192.168.50.20 192.168.50.30
#server 192.168.60.0 255.255.255.0
#route 192.168.50.0 255.255.255.0
#push "route 192.168.50.0 255.255.255.0"

ifconfig-pool-persist ipp.txt
client-to-client
#client-config-dir /etc/openvpn/ccd

keepalive 10 120
comp-lzo
persist-key
#persist-tun
status /var/log/openvpn-status.log
log /var/log/openvpn.log
verb 3

```
